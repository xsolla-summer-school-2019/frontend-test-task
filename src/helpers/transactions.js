import { currencyToUSD } from "./currencies";
import { getCountryName } from "./countries";

export function transformTransactionsArray(transactions) {
    return transactions.map(transactionTransformer);
}

function transactionTransformer(transaction) {
    let result = Object.assign({}, transaction);
    
    // converting to USD, so it is easier to implement sorting on currency field 
    result.payment_details.payment.amount = currencyToUSD(transaction.payment_details.payment).toFixed(2);
    result.payment_details.payment.currency = 'USD';
    
    // convert to full country name for proper sorting and representation
    // access via brackets cz of linter
    result["user"].country = getCountryName(transaction.user.country);
    
    // reduce information about purchase for easier representation
    result.purchase = purchaseReducer(transaction.purchase);
    
    return result;
}

function purchaseReducer(purchase) {
    for (const key of Object.keys(purchase)) {
        if (purchase[key]) {
            if (typeof purchase[key] === 'object' && purchase[key] !== null) {
                if (!purchase[key].hasOwnProperty('amount')) {
                    return `${key}: ${purchase[key]['name']}`;
                }
                if (purchase[key]['amount'] !== 0) {
                    return `${purchase[key]['amount']} ${purchase[key]['currency'] || purchase[key]['name']}`;
                }
            } else {
                return purchase[key];
            }
        }
    }
}
