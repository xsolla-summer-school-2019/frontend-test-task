import { currencyToUSD } from "./currencies";

export function constructProjectList(transactions) {
    let res = {};
    
    transactions.forEach(function(element) {
        if (!res[element.transaction.project.id]) {
            res[element.transaction.project.id] = {
                name: element.transaction.project.name,
                count: 1,
                total_value: currencyToUSD(element.payment_details.payment)
            };
        } else {
            res[element.transaction.project.id].count += 1;
            res[element.transaction.project.id].total_value += currencyToUSD(element.payment_details.payment);
        }
    });
    
    return res;
}

