import currencies from '../data/currencies.json';

export function convertToUSD(amount, currency) {
    return amount * currencies[currency];
}

export function currencyToUSD(payment) {
    return convertToUSD(payment.amount, payment.currency);
}
