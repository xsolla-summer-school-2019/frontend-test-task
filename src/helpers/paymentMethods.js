import { format } from 'date-fns';

export function getPaymentMethodsUsage(transactions) {
    return transactions.reduce(function (acc, transaction) {
        const paymentMethod = transaction.transaction.payment_method;
        
        if (acc[paymentMethod.id]) {
            acc[paymentMethod.id].dates.push(transaction.transaction.transfer_date);
            acc[paymentMethod.id].count += 1;
        } else {
            acc[paymentMethod.id] = {
                name: paymentMethod.name,
                dates: [transaction.transaction.transfer_date],
                count: 1
            };
        }
        
        return acc;
    }, {});
}


export function getPopularity(transactions) {
    const tmp = [...transactions].sort(
        function (a, b) {
            if (a.transaction.transfer_date < b.transaction.transfer_date) return -1;
            if (a.transaction.transfer_date > b.transaction.transfer_date) return 1;
            return 0;
        }
    );
    
    return tmp.reduce(function (acc, current) {
        const fullDate = new Date(current.transaction.transfer_date);
        const date = format(fullDate, 'DD.MM.YYYY HH:00');
        const paymentMethodName = current.transaction.payment_method.name;
        
        if (acc.length > 0) {
            if (acc[acc.length - 1].date === date) {
                if (acc[acc.length - 1][paymentMethodName])
                    acc[acc.length - 1][paymentMethodName] += 1;
                else
                    acc[acc.length - 1][paymentMethodName] = 1;
            } else {
                let temp = Object.assign({}, acc[acc.length - 1]);
                temp.date = date;
                acc.push(temp);
            }
        } else {
            acc.push({ 'date': date });
            acc[0][paymentMethodName] = 1;
        }
        
        return acc;
    }, []);
} 
