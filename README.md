# Xsolla Summer School 2019 Frontend test task

## Few design decisions explained
./data folder is used as mock data.
 There can be found .json files whose only purpose 
 is not to make any ajax requests in this project.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
Проверять вероятно удобно будет через это
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
